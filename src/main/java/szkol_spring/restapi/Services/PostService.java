package szkol_spring.restapi.Services;


import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import szkol_spring.restapi.Models.Comment;
import szkol_spring.restapi.Models.Post;
import szkol_spring.restapi.Repositories.CommentRepository;
import szkol_spring.restapi.Repositories.PostRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PostService {

    private static final int PAGE_SIZE = 20;
    private final PostRepository postRepository;
    private final CommentRepository commentRepository;

    public List<Post> getPosts(int page, Sort.Direction sort){
       return postRepository.findAllPosts(
               PageRequest.of(page, PAGE_SIZE, Sort.by(sort, "id") )
       );
    }

    public Post getPost(long id ){
        return postRepository.findById(id)
                .orElseThrow();
    }

    public List<Post> getPostsWithComments(int pageNumber, Sort.Direction sort) {
        List<Post> allPosts = postRepository.findAllPosts(PageRequest.of(pageNumber, PAGE_SIZE, Sort.by( sort, "id")));
        List<Long> ids = allPosts.stream()
                .map(Post::getId)
                .collect(Collectors.toList());
        List<Comment> comments = commentRepository.findAllByPostIdIn(ids);
        allPosts.forEach(post -> post.setCommentList(extractComments(comments, post.getId() )));
        return allPosts;
    }

    private List<Comment> extractComments(List<Comment> comments, long id) {
        return comments.stream()
                .filter(comment -> comment.getPostId() == id)
                .collect(Collectors.toList());
    }

    public Post addPost(Post post) {
     return postRepository.save(post);
    }

    @Transactional
    public Post editPost(Post post) {
        Post postEdited = postRepository.findById(post.getId()).orElseThrow();
        postEdited.setTitle(post.getTitle());
        postEdited.setContent(post.getContent());
        return postEdited;
    }


    public void deletePost(long id) {
        postRepository.deleteById(id);
    }
}
