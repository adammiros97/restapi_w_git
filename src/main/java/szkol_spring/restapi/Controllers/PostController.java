package szkol_spring.restapi.Controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import szkol_spring.restapi.Controllers.DTO.PostDTO;
import szkol_spring.restapi.Controllers.DTO.PostDTOMapper;
import szkol_spring.restapi.Models.Post;
import szkol_spring.restapi.Services.PostService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class PostController {

    private final PostService postService;


    @GetMapping("/posts")
    public List<PostDTO> getPosts(@RequestParam(required = false) Integer page, Sort.Direction sort)  {
        int pageNumber = page != null && page >= 0 ? page :0;
        Sort.Direction sortDirection = sort != null ? sort : Sort.Direction.DESC;
        return PostDTOMapper.mapToPostDTOs(postService.getPosts(page, sortDirection));
    }

    @GetMapping("/posts/comments")
    public List<Post> getPostsWithComments(@RequestParam(required = false) Integer page, Sort.Direction sort){
        int pageNumber = page != null && page >= 0 ? page :0;
        Sort.Direction sortDirection = sort != null ? sort : Sort.Direction.DESC;
        return postService.getPostsWithComments(pageNumber, sortDirection);
    }

    @GetMapping("/posts/{id}")
    public Post getSinglePost(@PathVariable long id){
        return postService.getPost(id);
    }

    @PostMapping("/posts")
    public Post addPost(@RequestBody Post post ){
        return postService.addPost(post);
    }

    @PutMapping("/posts")
    public Post editPost(@RequestBody Post post){
        return postService.editPost(post);
    }

    @DeleteMapping("/posts/{id}")
    public void deletePost(@PathVariable long id){
        postService.deletePost(id);
    }
}
