package szkol_spring.restapi.Controllers.DTO;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;


@Getter
@Setter
@Builder
public class PostDTO {
    private long id;
    private String title;
    private String content;
    private LocalDateTime created;
}
