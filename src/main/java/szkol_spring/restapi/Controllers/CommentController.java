package szkol_spring.restapi.Controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import szkol_spring.restapi.Models.Comment;
import szkol_spring.restapi.Services.CommentService;

@RestController
@RequiredArgsConstructor
public class CommentController {

    private final CommentService commentService;

    @PostMapping("/comments")
    public Comment addComment(@RequestBody Comment comment){
        return commentService.addComment(comment);
    }

    @GetMapping("/comments/{id}")
    public Comment getSingleComment(@PathVariable long id){
        return commentService.getComment(id);
    }

}
