package szkol_spring.restapi.Services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import szkol_spring.restapi.Models.Comment;
import szkol_spring.restapi.Repositories.CommentRepository;

@Service
@RequiredArgsConstructor
public class CommentService {

    private final CommentRepository commentRepository;


    public Comment addComment(Comment comment) {
       return commentRepository.save(comment);
    }

    public Comment getComment(long id) {
        return commentRepository.findById(id).orElseThrow();
    }
}
