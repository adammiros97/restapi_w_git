CREATE TABLE POST(
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(400) NOT NULL,
    content VARCHAR(2000) NULL,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE COMMENT (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    post_id BIGINT not null,
    content varchar(2000) null,
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

alter table comment
    add constraint comment_post_id
    foreign key (post_id) references post(id)